<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Class Admin
 * @package App\Http\Middleware
 * @author David Okhikyan <david.okhikyan@gmail.com>
 */
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        /**
         * if user is not logged in send to 404 page
         */
        if (!Auth::guard($guard)->check()) {
            abort('404');
        }
        /**
         * if user is not admin send to 404 page
         */
        if (Auth::user()->role != User::ADMIN_ROLE){
            abort('404');
        }

        return $next($request);
    }
}
