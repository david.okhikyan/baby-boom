<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix' => 'admin','middleware' => 'admin'] , function(){
    /**
     * dashboard routes
     */
    Route::get('/' , [
        'as'    => 'admin.getIndex',
        'uses'  => 'Admin\DashboardController@index']);

    /**
     * category routes
     */
    Route::group(['prefix' => 'category'] , function(){

        Route::get('/' , [
            'as'   => 'category.getIndex',
            'uses' => 'Admin\CategoryController@index'
        ]);

        Route::get('/unique' , [
            'as'   => 'category.getUniqueIndex',
            'uses' => 'Admin\CategoryController@indexUnique'
        ]);

        Route::get('/create' , [
            'as'   => 'category.getCreate',
            'uses' => 'Admin\CategoryController@create'
        ]);

        Route::post('/create' , [
            'as'   => 'category.postCreate',
            'uses' => 'Admin\CategoryController@store'
        ]);

        Route::post('/create-unique' , [
            'as'   => 'category.postCreateUnique',
            'uses' => 'Admin\CategoryController@storeUnique'
        ]);

        Route::get('/update/{id}' , [
            'as'   => 'category.getUpdate',
            'uses' => 'Admin\CategoryController@edit'
        ]);

        Route::post('/update' , [
            'as'   => 'category.postUpdate',
            'uses' => 'Admin\CategoryController@update'
        ]);

        Route::post('/update-unique' , [
            'as'   => 'category.postUpdateUnique',
            'uses' => 'Admin\CategoryController@updateUnique'
        ]);

        Route::get('/delete/{id}' , [
            'as'   => 'category.getDelete',
            'uses' => 'Admin\CategoryController@destroy'
        ]);

        Route::get('/delete/unique/{id}' , [
            'as'   => 'category.getDeleteUnique',
            'uses' => 'Admin\CategoryController@destroyUnique'
        ]);

        Route::post('/delete/option' , [
            'as'   => 'category.postDeleteOption',
            'uses' => 'Admin\CategoryController@destroyOption'
        ]);
    });


    /**
     * product routes
     */
    Route::group(['prefix' => 'product'] , function(){
        Route::get('/' , [
            'as'   => 'product.getIndex',
            'uses' => 'Admin\ProductController@index'
        ]);

        Route::get('/create' , [
            'as'   => 'product.getCreate',
            'uses' => 'Admin\ProductController@create'
        ]);

        Route::post('/create' , [
            'as'   => 'product.postCreate',
            'uses' => 'Admin\ProductController@store'
        ]);

        Route::get('/update/{id}' , [
            'as'   => 'product.getEdit',
            'uses' => 'Admin\ProductController@edit'
        ]);

        Route::post('/update' , [
            'as'   => 'product.postUpdate',
            'uses' => 'Admin\ProductController@update'
        ]);
        Route::get('/delete/{id}' , [
            'as'   => 'product.getDelete',
            'uses' => 'Admin\ProductController@destroy'
        ]);

        Route::post('/upload-image' , [
            'as' => 'product.PostUploadImage',
            'uses' => 'Admin\ProductController@uploadImage'
        ]);

        Route::post('/import' , [
            'as' => 'product.postImport',
            'uses' => 'Admin\ProductController@import'
        ]);

        Route::post('/update-price' , [
            'as' => 'product.postUpdatePrice',
            'uses' => 'Admin\ProductController@updatePrice'
        ]);

    });

    /**
     * users routes
     */
    Route::group(['prefix' => 'user'] , function(){
        Route::get('/' , [
            'as'   => 'user.getIndex',
            'uses' => 'Admin\UserController@index'
        ]);
        Route::post('/create' , [
            'as'   => 'user.postCreate',
            'uses' => 'Admin\UserController@create'
        ]);

        Route::post('/update' , [
            'as'   => 'user.postUpdate',
            'uses' => 'Admin\UserController@update'
        ]);
        Route::get('/delete/{id}' , [
            'as'   => 'user.getDelete',
            'uses' => 'Admin\UserController@destroy'
        ]);
    });

    /**
     * orders routes
     */
    Route::group(['prefix' => 'order'] , function(){
        Route::get('/' , [
            'as'   => 'order.getIndex',
            'uses' => 'Admin\OrderController@index'
        ]);
        Route::post('/create' , [
            'as'   => 'order.postCreate',
            'uses' => 'Admin\OrderController@store'
        ]);
        Route::post('/update' , [
            'as'   => 'order.postUpdate',
            'uses' => 'Admin\OrderController@update'
        ]);
        Route::get('/delete/{id}' , [
            'as'   => 'order.getDelete',
            'uses' => 'Admin\OrderController@destroy'
        ]);
        Route::get('/export/{id}' , [
            'as'   => 'order.getExport',
            'uses' => 'Admin\OrderController@export'
        ]);
    });
});


